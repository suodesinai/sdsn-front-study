import { User } from "../entity/user";

export interface LoginInfo {
  username: string;
  password: string;
}

export interface UserWithPromises extends User {
  promises: string[];
  org: any[];
  menus: any[];
  roles: any[];
}
