import Koa from "koa";
import roleMiddleware from "../middleware/role";
import { response } from "../utils/http";
import { User } from "../entity/user";
import {
  taskCreate,
  taskDelete,
  taskDetail,
  taskPage,
  taskRemarkCreate,
  taskRemarkDelete,
  taskRemarkUpdate,
  taskRemarks,
  taskUpdate,
} from "../servers/task/task";

const Router = require("koa-router");

const router = new Router();

/**
 * @swagger
 * /task-page: #接口访问路径
 *   get: #请求方式
 *     summary: 任务列表  #任务列表接口信息
 *     description: 任务列表 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: pageIndex #参数名字
 *         description: 页码 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: pageSize #参数名字
 *         description: 数量 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 获取任务列表
router.get(
  "/task-page",
  roleMiddleware("task_page"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    ctx.body = response(
      200,
      await taskPage(ctx.request.query as any, { user_id: userInfo.user_id }),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /task-add: #接口访问路径
 *   post: #请求方式
 *     summary: 添加任务 #添加任务接口信息
 *     description: 添加任务 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: task_name #参数名字
 *         description: 任务名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: task_content #参数名字
 *         description: 任务内容 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: task_status #参数名字
 *         description: 任务状态 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: integer #参数类型
 *       - name: task_type #参数名字
 *         description: 任务类型 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: integer #参数类型
 *       - name: performer #参数名字
 *         description: 任务参与者 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: integer #参数类型
 *       - name: task_category #参数名字
 *         description: 任务分类 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: integer #参数类型
 *       - name: task_performer #参数名字
 *         description: 任务执行者 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: integer #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 添加任务
router.post(
  "/task-add",
  roleMiddleware("task_add"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    ctx.body = response(
      200,
      await taskCreate(ctx.request.body, userInfo.user_id),
      "操作成功！",
    );
  },
);

/**
 * @swagger
 * /task-update: #接口访问路径
 *   put: #请求方式
 *     summary: 修改任务 #修改任务接口信息
 *     description: 修改任务 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: task_id #参数名字
 *         description: 任务id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: task_name #参数名字
 *         description: 任务名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: task_content #参数名字
 *         description: 任务内容 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: task_status #参数名字
 *         description: 任务状态 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: integer #参数类型
 *       - name: task_type #参数名字
 *         description: 任务类型 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: integer #参数类型
 *       - name: performer #参数名字
 *         description: 任务参与者 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: integer #参数类型
 *       - name: task_category #参数名字
 *         description: 任务分类 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: integer #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 修改任务
router.put(
  "/task-update",
  roleMiddleware("task_update"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    ctx.body = response(
      200,
      await taskUpdate(ctx.request.body, userInfo.user_id),
      "操作成功！",
    );
  },
);

/**
 * @swagger
 * /task-detail: #接口访问路径
 *   get: #请求方式
 *     summary: 任务详情  #任务详情接口信息
 *     description: 任务详情 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: id #参数名字
 *         description: 任务ID #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 获取任务列表
router.get(
  "/task-detail",
  roleMiddleware("task_detail"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    if (!ctx.request.query.id) {
      throw new Error("任务ID不能为空！");
    }
    ctx.body = response(
      200,
      await taskDetail(Number(ctx.request.query.id), Number(userInfo.user_id)),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /task-delete: #接口访问路径
 *   delete: #请求方式
 *     summary: 删除任务  #删除任务接口信息
 *     description: 删除任务 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: id #参数名字
 *         description: 任务备注ID #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 删除任务
router.delete(
  "/task-delete",
  roleMiddleware("task_delete"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    if (!ctx.request.query.id) {
      throw new Error("任务ID不能为空！");
    }
    ctx.body = response(
      200,
      await taskDelete(Number(ctx.request.query.id), Number(userInfo.user_id)),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /task-remarks: #接口访问路径
 *   get: #请求方式
 *     summary: 获取任务备注列表  #任务备注接口信息
 *     description: 任务备注 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: id #参数名字
 *         description: 任务ID #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 获取任务备注列表
router.get(
  "/task-remarks",
  roleMiddleware("task_remarks"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    if (!ctx.request.query.id) {
      throw new Error("任务ID不能为空！");
    }
    ctx.body = response(
      200,
      await taskRemarks(Number(ctx.request.query.id), Number(userInfo.user_id)),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /task-remark-add: #接口访问路径
 *   post: #请求方式
 *     summary: 添加任务备注  #任务备注接口信息
 *     description: 添加任务备注 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: task_id #参数名字
 *         description: 任务ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: task_content #参数名字
 *         description: 任务备注 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 添加任务备注
router.post(
  "/task-remark-add",
  roleMiddleware("task_remark_add"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;

    ctx.body = response(
      200,
      await taskRemarkCreate(ctx.request.body, Number(userInfo.user_id)),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /task-remark-delete: #接口访问路径
 *   delete: #请求方式
 *     summary: 删除任务备注  #删除任务备注接口信息
 *     description: 删除任务备注 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: id #参数名字
 *         description: 任务备注ID #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 删除任务备注
router.delete(
  "/task-remark-delete",
  roleMiddleware("task_remark_delete"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    if (!ctx.request.query.id) {
      throw new Error("任务备注ID不能为空！");
    }
    ctx.body = response(
      200,
      await taskRemarkDelete(
        Number(ctx.request.query.id),
        Number(userInfo.user_id),
      ),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /task-remark-update: #接口访问路径
 *   put: #请求方式
 *     summary: 修改任务备注  #任务备注接口信息
 *     description: 修改任务备注 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: remark_id #参数名字
 *         description: 任务备注ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: task_id #参数名字
 *         description: 任务ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: task_content #参数名字
 *         description: 任务备注内容 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 修改任务备注
router.put(
  "/task-remark-update",
  roleMiddleware("task_remark_update"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;

    ctx.body = response(
      200,
      await taskRemarkUpdate(ctx.request.body, Number(userInfo.user_id)),
      "ok！",
    );
  },
);
export default router;
