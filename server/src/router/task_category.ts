import Koa from "koa";
import roleMiddleware from "../middleware/role";
import { response } from "../utils/http";
import { User } from "../entity/user";
import {
  taskCategoryCreate,
  taskCategoryDelByIds,
  taskCategoryDetail,
  taskCategoryUpdate,
} from "../servers/taskCategory/taskCategory";

const Router = require("koa-router");

const router = new Router();

/**
 * @swagger
 * /task-category-add: #接口访问路径
 *   post: #请求方式
 *     summary: 添加任务分类 #添加任务分类接口信息
 *     description: 添加任务分类 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: category_name #参数名字
 *         description: 分类名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: category_describe #参数名字
 *         description: 描述 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: user_ids #参数名字
 *         description: 用户权限 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: org_ids #参数名字
 *         description: 组织权限 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 添加任务分类
router.post(
  "/task-category-add",
  roleMiddleware("task_category_add"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    ctx.body = response(
      200,
      await taskCategoryCreate(ctx.request.body, userInfo.user_id),
      "操作成功！",
    );
  },
);

/**
 * @swagger
 * /task-category-detail: #接口访问路径
 *   get: #请求方式
 *     summary: 任务分类详情 #任务分类详情接口信息
 *     description: 任务分类详情 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: id #参数名字
 *         description: 分类id #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 任务分类详情
router.get(
  "/task-category-detail",
  roleMiddleware("task_category_detail"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    if (!ctx.request.query.id) {
      throw new Error("参数缺失！");
    }
    ctx.body = response(
      200,
      await taskCategoryDetail(Number(ctx.request.query.id), userInfo.user_id),
      "操作成功！",
    );
  },
);

/**
 * @swagger
 * /task-category-update: #接口访问路径
 *   put: #请求方式
 *     summary: 任务分类修改 #任务分类修改接口信息
 *     description: 任务分类修改 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: category_id #参数名字
 *         description: 分类id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: category_name #参数名字
 *         description: 分类名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: category_describe #参数名字
 *         description: 描述 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: user_ids #参数名字
 *         description: 用户权限 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: org_ids #参数名字
 *         description: 组织权限 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 修改任务分类
router.put(
  "/task-category-update",
  roleMiddleware("task_category_update"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    ctx.body = response(
      200,
      await taskCategoryUpdate(ctx.request.body, userInfo.user_id),
      "操作成功！",
    );
  },
);

/**
 * @swagger
 * /task-category-delete: #接口访问路径
 *   delete: #请求方式
 *     summary: 删除任务分类 #删除任务分类接口信息
 *     description: 删除任务分类 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: ids #参数名字
 *         description: 分类id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 删除任务分类
router.delete(
  "/task-category-delete",
  roleMiddleware("task_category_delete"),
  async (ctx: Koa.Context) => {
    if (!ctx.request.query.ids) {
      throw new Error("ids不能为空！");
    }

    const userInfo = ctx.state.user as User;
    ctx.body = response(
      200,
      await taskCategoryDelByIds(
        (ctx.request.query.ids as string).split(","),
        userInfo.user_id,
      ),
      "操作成功！",
    );
  },
);

export default router;
