import Koa from "koa";
import roleMiddleware from "../middleware/role";
import { User } from "../entity/user";
import {
  netdiskCreate,
  netdiskList,
  netdiskUpload,
} from "../servers/netdisk/netdisk";
import { response } from "../utils/http";
import { upload } from "../servers/netdisk/upload";
const Router = require("koa-router");

const router = new Router();

/**
 * @swagger
 * /netdisk-add: #接口访问路径
 *   post: #请求方式
 *     summary: 添加文件/文件夹  #添加文件/文件夹接口信息
 *     description: 添加分类 #接口描述
 *     tags: [网盘] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: file_name #参数名字
 *         description: 文件/文件夹名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: parent_id #参数名字
 *         description: 所属父级ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.post(
  "/netdisk-add",
  roleMiddleware("netdisk_add"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;

    ctx.body = response(
      200,
      await netdiskCreate(ctx.request.body, String(userInfo.user_id)),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /netdisk-upload: #接口访问路径
 *   post: #请求方式
 *     summary: 上传文件(单个)  #上传文件接口信息
 *     description: 上传文件 #接口描述
 *     tags: [网盘] #接口分组
 *     produces:
 *       - application/x-www-form-urlencoded
 *     parameters: #传递参数
 *       - name: file #参数名字
 *         description: 文件/文件夹名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: file #参数类型
 *       - name: parent_id #参数名字
 *         description: 所属父级ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.post(
  "/netdisk-upload",
  roleMiddleware("netdisk_upload"),
  upload.single("file"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;

    ctx.body = response(
      200,
      await netdiskUpload(ctx.request.body, ctx.file, userInfo.user_id),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /netdisk-page: #接口访问路径
 *   get: #请求方式
 *     summary: 获取网盘文件列表  #接口信息
 *     description: 文件列表 #接口描述
 *     tags: [网盘] #接口分组
 *     produces:
 *       - application/x-www-form-urlencoded
 *     parameters: #传递参数
 *       - name: pageIndex #参数名字
 *         description: 页码 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: file #参数类型
 *       - name: pageSize #参数名字
 *         description: 数据 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: parent_id #参数名字
 *         description: 数据 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.get(
  "/netdisk-page",
  roleMiddleware("netdisk_page"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;

    ctx.body = response(
      200,
      await netdiskList(ctx.request.query, userInfo.user_id),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /netdisk-page: #接口访问路径
 *   get: #请求方式
 *     summary: 获取网盘文件列表  #接口信息
 *     description: 文件列表 #接口描述
 *     tags: [网盘] #接口分组
 *     produces:
 *       - application/x-www-form-urlencoded
 *     parameters: #传递参数
 *       - name: pageIndex #参数名字
 *         description: 页码 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: file #参数类型
 *       - name: pageSize #参数名字
 *         description: 数据 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: parent_id #参数名字
 *         description: 数据 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.get(
  "/netdisk-page",
  roleMiddleware("netdisk_page"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;

    ctx.body = response(
      200,
      await netdiskList(ctx.request.query, userInfo.user_id),
      "ok！",
    );
  },
);

export default router;
