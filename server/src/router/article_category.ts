import Koa from "koa";
import { response } from "../utils/http";
import roleMiddleware from "../middleware/role";

import { validatorData } from "../db/utils";
import {
  articleCategoryCreate,
  articleCategoryUpdate,
  getArticleCategoryTree,
} from "../servers/articleCategory/articleCategory";
import { formatPageFileterData } from "../utils/pages";
import { CreateCategory } from "../servers/articleCategory/type";
import { ArticleCategory } from "../entity/article_category";
import { User } from "../entity/user";
const Router = require("koa-router");

const router = new Router();

/**
 * @swagger
 * /article-category-tree: #接口访问路径
 *   get: #请求方式
 *     summary: 分类树  #文章分类树接口信息
 *     description: 文章分类树 #接口描述
 *     tags: [文章] #接口分组
 *     produces:
 *       - application/json
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.get(
  "/article-category-tree",
  roleMiddleware("article_category_tree"),
  async (ctx: Koa.Context) => {
    ctx.body = response(
      200,
      await getArticleCategoryTree(ctx.state.user.user_id),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /article-category-update: #接口访问路径
 *   put: #请求方式
 *     summary: 修改分类  #文章分类树接口信息
 *     description: 修改文章分类 #接口描述
 *     tags: [文章] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: category_id #参数名字
 *         description: 所属分类ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: integer #参数类型
 *       - name: category_name #参数名字
 *         description: 所属分类名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: parent_id #参数名字
 *         description: 所属分类父级ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: role_id #参数名字
 *         description: 角色ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: role_name #参数名字
 *         description: 角色名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.put(
  "/article-category-update",
  roleMiddleware("article_category_update"),

  async (ctx: Koa.Context) => {
    const categoryData = formatPageFileterData(ctx.request.body, [
      "category_id",
      "category_name",
      "parent_id",
      "role_id",
    ]);
    ctx.body = response(
      200,
      await articleCategoryUpdate(
        categoryData as unknown as CreateCategory,
        ctx.state.user.user_id,
      ),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /article-category-add: #接口访问路径
 *   post: #请求方式
 *     summary: 添加分类  #文章分类树接口信息
 *     description: 添加文章分类 #接口描述
 *     tags: [文章] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: category_name #参数名字
 *         description: 所属分类名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: parent_id #参数名字
 *         description: 所属分类父级ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: role_id #参数名字
 *         description: 角色ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: role_name #参数名字
 *         description: 角色名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.post(
  "/article-category-add",
  roleMiddleware("article_category_add"),
  async (ctx: Koa.Context) => {
    const categoryData = formatPageFileterData(ctx.request.body, [
      "category_name",
      "parent_id",
      "role_id",
      "role_name",
    ]);

    categoryData.create_user = ctx.state.user.user_id;
    categoryData.parent_id = categoryData.parent_id || 0;

    await validatorData(categoryData, ArticleCategory);

    ctx.body = response(
      200,
      await articleCategoryCreate(categoryData as unknown as CreateCategory),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /article-category-delete: #接口访问路径
 *   delete: #请求方式
 *     summary: 删除文章分类  #删除任务分类接口信息
 *     description: 删除文章分类 #接口描述
 *     tags: [任务] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: id #参数名字
 *         description: 任务备注ID #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 删除文章分类
router.delete(
  "/article-category-delete",
  roleMiddleware("article_category_delete"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    if (!ctx.request.query.id) {
      throw new Error("该功能尚未开放！");
    }
  },
);

export default router;
