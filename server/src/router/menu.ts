import Koa from "koa";
import { response } from "../utils/http";
import roleMiddleware from "../middleware/role";
import {
  menuAdd,
  menuDelByUserIds,
  menuFindOne,
  menuPageByParams,
  menuTree,
  menuUpdate,
} from "../servers/menu/menu";
import { Menu } from "../entity/menu";
import { validatorData } from "../db/utils";
import { User } from "../entity/user";
const Router = require("koa-router");

const router = new Router();

/**
 * @swagger
 * /menu-page: #接口访问路径(拼接上之前swagger.js中的host蛇者)
 *   get: #请求方式
 *     summary: 菜单分页  #菜单信息接口信息
 *     description: 菜单信息 #接口描述
 *     tags: [菜单] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: parent_id #参数名字
 *         description: 父级菜单id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.get(
  "/menu-page",
  roleMiddleware("menu_page"),
  async (ctx: Koa.Context) => {
    ctx.body = response(
      200,
      await menuPageByParams(ctx.request.query as any, ctx.state.user.user_id),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /menu-tree: #接口访问路径(拼接上之前swagger.js中的host蛇者)
 *   get: #请求方式
 *     summary: 菜单树  #菜单树接口信息
 *     description: 菜单树 #接口描述
 *     tags: [菜单] #接口分组
 *     produces:
 *       - application/json
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 获取菜单树
router.get(
  "/menu-tree",
  roleMiddleware("menu_tree"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    ctx.body = response(
      200,
      await menuTree({ user_id: userInfo.user_id }),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /menu-add: #接口访问路径(拼接上之前swagger.js中的host蛇者)
 *   post: #请求方式
 *     summary: 添加菜单 #添加菜单接口信息
 *     description: 添加菜单 #接口描述
 *     tags: [菜单] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: parent_id #参数名字
 *         description: 父级菜单id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: menu_name #参数名字
 *         description: 菜单名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: menu_code #参数名字
 *         description: 菜单编码 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: menu_path #参数名字
 *         description: 菜单url #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 添加菜单
router.post(
  "/menu-add",
  roleMiddleware("menu_add"),
  async (ctx: Koa.Context) => {
    const menuInfo = ctx.request.body as Menu;

    await validatorData(menuInfo, Menu);

    const menu = await menuFindOne({ menu_code: menuInfo.menu_code });

    if (menu) {
      ctx.body = response(500, null, `已存在${menuInfo.menu_code}！`);
      return;
    }

    await menuAdd(menuInfo);

    ctx.body = response(200, null, "操作成功！");
  },
);

/**
 * @swagger
 * /menu-update: #接口访问路径(拼接上之前swagger.js中的host蛇者)
 *   post: #请求方式
 *     summary: 修改菜单 #添加菜单接口信息
 *     description: 修改菜单 #接口描述
 *     tags: [菜单] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: menu_id #参数名字
 *         description: 菜单id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: parent_id #参数名字
 *         description: 父级菜单id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: menu_name #参数名字
 *         description: 菜单名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: menu_code #参数名字
 *         description: 菜单编码 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: menu_path #参数名字
 *         description: 菜单url #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 修改菜单
router.put(
  "/menu-update",
  roleMiddleware("menu_update"),
  async (ctx: Koa.Context) => {
    const menuInfo = ctx.request.body as Menu;

    if (!menuInfo.menu_id) {
      ctx.body = response(500, null, "菜单ID不能为空！");
      return;
    }

    await validatorData(menuInfo, Menu);

    const menu = await menuFindOne({ menu_id: menuInfo.menu_id });

    if (!menu) {
      ctx.body = response(500, null, `不存在该菜单！`);
      return;
    }

    await menuUpdate(menuInfo);

    ctx.body = response(200, null, "操作成功！");
  },
);

/**
 * @swagger
 * /menu-delete: #接口访问路径
 *   delete: #请求方式
 *     summary: 删除菜单  #接口信息
 *     description: 删除菜单 #接口描述
 *     tags: [菜单] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: ids #参数名字
 *         description: 菜单id，逗号隔开，支持批量删除 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 *       401:
 *         description: 未登录
 */
// 删除菜单
router.delete(
  "/menu-delete",
  roleMiddleware("menu_delete"),
  async (ctx: Koa.Context) => {
    const ids = ctx.request.query.ids as string;
    if (!ids) {
      ctx.body = response(500, null, `id不能为空！`);
      return;
    }

    await menuDelByUserIds(ids.split(","));

    ctx.body = response(200, null, "操作成功！");
  },
);

export default router;
