import Koa from "koa";
import roleMiddleware from "../middleware/role";
import { response } from "../utils/http";
import { User } from "../entity/user";
import {
  orgAdd,
  orgDelByUserIds,
  orgFindOne,
  orgTree,
  orgUpdate,
} from "../servers/org/org";
import { validatorData } from "../db/utils";
import { Org } from "../entity/org";

const Router = require("koa-router");

const router = new Router();

/**
 * @swagger
 * /org-tree: #接口访问路径
 *   get: #请求方式
 *     summary: 组织树  #组织树接口信息
 *     description: 组织树 #接口描述
 *     tags: [组织] #接口分组
 *     produces:
 *       - application/json
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 获取组织树
router.get(
  "/org-tree",
  roleMiddleware("org_tree"),
  async (ctx: Koa.Context) => {
    const userInfo = ctx.state.user as User;
    ctx.body = response(
      200,
      await orgTree({ user_id: userInfo.user_id }),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /org-add: #接口访问路径
 *   post: #请求方式
 *     summary: 添加组织 #添加组织接口信息
 *     description: 添加组织 #接口描述
 *     tags: [组织] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: parent_id #参数名字
 *         description: 父级组织id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: org_name #参数名字
 *         description: 组织名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: org_code #参数名字
 *         description: 组织编码 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 添加组织
router.post("/org-add", roleMiddleware("org_add"), async (ctx: Koa.Context) => {
  const orgInfo = ctx.request.body as Org;

  await validatorData(orgInfo, Org);

  await orgAdd(orgInfo);

  ctx.body = response(200, null, "操作成功！");
});

/**
 * @swagger
 * /org-update: #接口访问路径
 *   put: #请求方式
 *     summary: 修改组织 #添加组织接口信息
 *     description: 修改组织 #接口描述
 *     tags: [组织] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: org_id #参数名字
 *         description: 组织id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: parent_id #参数名字
 *         description: 父级组织id #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: org_name #参数名字
 *         description: 组织名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: org_code #参数名字
 *         description: 组织编码 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 修改组织
router.put(
  "/org-update",
  roleMiddleware("org_update"),
  async (ctx: Koa.Context) => {
    const orgInfo = ctx.request.body as Org;

    if (!orgInfo.org_id) {
      ctx.body = response(500, null, "组织ID不能为空！");
      return;
    }

    await validatorData(orgInfo, Org);

    const menu = await orgFindOne({ org_id: orgInfo.org_id });

    if (!menu) {
      ctx.body = response(500, null, `不存在该组织！`);
      return;
    }

    await orgUpdate(orgInfo);

    ctx.body = response(200, null, "操作成功！");
  },
);

/**
 * @swagger
 * /org-delete: #接口访问路径
 *   delete: #请求方式
 *     summary: 删除组织  #接口信息
 *     description: 删除组织 #接口描述
 *     tags: [组织] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: ids #参数名字
 *         description: 组织id，逗号隔开，支持批量删除 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 *       401:
 *         description: 未登录
 */
// 删除组织
router.delete(
  "/org-delete",
  roleMiddleware("org_delete"),
  async (ctx: Koa.Context) => {
    const ids = ctx.request.query.ids as string;
    if (!ids) {
      ctx.body = response(500, null, `id不能为空！`);
      return;
    }

    if (ids.split(",").includes("1")) {
      throw new Error("根组织不能删除！");
    }

    await orgDelByUserIds(ids.split(","));

    ctx.body = response(200, null, "操作成功！");
  },
);

export default router;
