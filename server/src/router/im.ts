import Koa from "koa";
import { response } from "../utils/http";
import roleMiddleware from "../middleware/role";
import { menuPageByParams } from "../servers/menu/menu";
const Router = require("koa-router");

const router = new Router();

router.get(
  "/im-talk-create",
  roleMiddleware("im-talk-create"),
  async (ctx: Koa.Context) => {
    // createTalk;
  },
);

export default router;
