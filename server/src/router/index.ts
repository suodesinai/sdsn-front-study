const bodyParser = require("koa-bodyparser");
const { koaSwagger } = require("koa2-swagger-ui");
const swagger = require("../swagger/index"); //存放swagger.js的地方
import Koa from "koa";
import useMiddleware from "../middleware/log";
import useJwt from "../middleware/jwt";
import userController from "./user";
import roleController from "./role";
import menuController from "./menu";
import articleController from "./article";
import articleCategoryController from "./article_category";
import orgController from "./org";
import taskController from "./task";
import taskCategoryController from "./task_category";
import netdiskController from "./netdisk";
import errorMiddleware from "../middleware/error";
export function initRouter(app: Koa<Koa.DefaultState, Koa.DefaultContext>) {
  // 错误处理
  app.on("error", (err) => {
    // console.log(2222,err);
  });
  // 异常错误拦截
  app.use(errorMiddleware);
  // swagger
  app.use(swagger.routes()).use(swagger.allowedMethods());
  app.use(
    koaSwagger({
      routePrefix: "/swagger", // 接口文档访问地址
      swaggerOptions: {
        url: "/swagger.json", //生成json的访问路径
      },
    }),
  );
  //body
  app.use(bodyParser());
  //日志
  app.use(useMiddleware);
  //jwt
  app.use(useJwt);
  //路由
  app.use(userController.routes()).use(userController.allowedMethods()); // 用户
  app.use(roleController.routes()).use(menuController.allowedMethods()); // 角色
  app.use(menuController.routes()).use(menuController.allowedMethods()); // 菜单
  app.use(orgController.routes()).use(orgController.allowedMethods()); // 组织
  app.use(taskController.routes()).use(taskController.allowedMethods()); // 任务
  app.use(netdiskController.routes()).use(netdiskController.allowedMethods()); // 网盘
  app
    .use(taskCategoryController.routes())
    .use(taskCategoryController.allowedMethods()); // 任务分类
  app
    .use(articleCategoryController.routes())
    .use(articleCategoryController.allowedMethods()); // 文章分类
  app.use(articleController.routes()).use(articleController.allowedMethods()); // 文章
}
