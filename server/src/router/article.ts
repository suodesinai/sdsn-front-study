import Koa from "koa";
import { response } from "../utils/http";
import roleMiddleware from "../middleware/role";
import {
  articleCreate,
  articleDelByUserIds,
  articleUpdate,
  getArticlesByRole,
} from "../servers/article/article";
import { ArticleRoleQuery } from "../servers/article/type";
import { Article } from "../entity/article";
const Router = require("koa-router");

const router = new Router();

/**
 * @swagger
 * /article-page: #接口访问路径
 *   get: #请求方式
 *     summary: 文章分页  #菜单信息接口信息
 *     description: 文章分页 #接口描述
 *     tags: [文章] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: category_id #参数名字
 *         description: 所属分类ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.get(
  "/article-page",
  roleMiddleware("article_page"),
  async (ctx: Koa.Context) => {
    ctx.body = response(
      200,
      await getArticlesByRole(
        ctx.request.query as ArticleRoleQuery,
        ctx.state.user.user_id,
      ),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /article-create: #接口访问路径
 *   post: #请求方式
 *     summary: 新增文章  #文章信息接口信息
 *     description: 新增文章 #接口描述
 *     tags: [文章] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: title #参数名字
 *         description: 标题 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: content #参数名字
 *         description: 文章内容 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: article_thumb #参数名字
 *         description: 缩略图 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: category_id #参数名字
 *         description: 所属分类 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: files #参数名字
 *         description: 附件 #参数信息，逗号隔开
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.post(
  "/article-create",
  roleMiddleware("article_create"),
  async (ctx: Koa.Context) => {
    ctx.body = response(
      200,
      await articleCreate(
        ctx.request.body as Article & { files: string },
        ctx.state.user.user_id,
      ),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /article-update: #接口访问路径
 *   put: #请求方式
 *     summary: 修改文章  #文章信息接口信息
 *     description: 修改文章 #接口描述
 *     tags: [文章] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: article_id #参数名字
 *         description: 文章标题 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: title #参数名字
 *         description: 标题 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: content #参数名字
 *         description: 文章内容 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: article_thumb #参数名字
 *         description: 缩略图 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: category_id #参数名字
 *         description: 所属分类 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: files #参数名字
 *         description: 附件 #参数信息，逗号隔开
 *         in: formData #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
router.put(
  "/article-update",
  roleMiddleware("article_update"),
  async (ctx: Koa.Context) => {
    ctx.body = response(
      200,
      await articleUpdate(
        ctx.request.body as Article & { files: string },
        ctx.state.user.user_id,
      ),
      "ok！",
    );
  },
);

/**
 * @swagger
 * /artcicle-delete: #接口访问路径
 *   delete: #请求方式
 *     summary: 删除文章  #接口信息
 *     description: 删除文章 #接口描述
 *     tags: [文章] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: ids #参数名字
 *         description: 文章id，逗号隔开，支持批量删除 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 *       401:
 *         description: 未登录
 */
// 删除组织
router.delete(
  "/article-delete",
  roleMiddleware("article_delete"),
  async (ctx: Koa.Context) => {
    const ids = ctx.request.query.ids as string;
    if (!ids) {
      ctx.body = response(500, null, `id不能为空！`);
      return;
    }

    await articleDelByUserIds(ids.split(","));

    ctx.body = response(200, null, "操作成功！");
  },
);

export default router;
