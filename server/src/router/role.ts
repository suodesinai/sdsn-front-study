import Koa from "koa";
import { response } from "../utils/http";
import roleMiddleware from "../middleware/role";
import { validatorData } from "../db/utils";
import {
  roleAdd,
  roleDelByRoleIds,
  roleFindOneByRoleId,
  rolePage,
  roleUpdate,
} from "../servers/role/role";
import { Role } from "../entity/role";
const Router = require("koa-router");

const router = new Router();

/**
 * @swagger
 * /role-page: #接口访问路径
 *   get: #请求方式
 *     summary: 角色分页  #接口信息
 *     description: 角色分页 #接口描述
 *     tags: [角色] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: pageIndex #参数名字
 *         description: 页码 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: false #是否是必传参数
 *         type: string #参数类型
 *       - name: pageSize
 *         description: 页数
 *         type: string
 *         in: query
 *         required: false
 *     responses: #返回结果
 *       200:
 *         description: 成功
 *         schema:
 *          type: object
 *          properties:
 *              records:
 *                type: array
 *                items:
 *                  type: object
 *                  properties:
 *                    role_id:
 *                      type: integer
 *                      description: 角色id
 *                    role_name:
 *                      type: string
 *                      description: 角色名称
 *                    role_code:
 *                      type: string
 *                      description: 角色码
 *                    create_time:
 *                      type: string
 *                      description: 创建时间
 *              total:
 *                type: integer
 *                properties: 总数
 *       401:
 *         description: 未登录
 */

// 角色分页列表
router.get("/role-page", async (ctx: Koa.Context) => {
  const page = await rolePage(ctx.request.query as unknown as any);
  ctx.body = response(200, page, "操作成功！");
});

/**
 * @swagger
 * /role-add: #接口访问路径
 *   post: #请求方式
 *     summary: 添加角色  #接口信息
 *     description: 添加角色 #接口描述
 *     tags: [角色] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: role_name #参数名字
 *         description: 角色名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: role_code
 *         description: 角色权限码
 *         type: string
 *         in: formData
 *         required: false
 *       - name: menu_ids
 *         description: 角色权限菜单
 *         type: string
 *         in: formData
 *         required: false
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 添加角色
router.post(
  "/role-add",
  roleMiddleware("role_add"),
  async (ctx: Koa.Context) => {
    const roleInfo = ctx.request.body as Role;
    if (!roleInfo.role_name) {
      ctx.body = response(500, null, `角色名不能为空！`);
      return;
    }

    await roleAdd(roleInfo);

    ctx.body = response(200, null, "操作成功！");
  },
);

/**
 * @swagger
 * /role-update: #接口访问路径
 *   put: #请求方式
 *     summary: 修改角色  #接口信息
 *     description: 修改角色 #接口描述
 *     tags: [角色] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: role_id #参数名字
 *         description: 角色ID #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: integer #参数类型
 *       - name: role_name #参数名字
 *         description: 角色名称 #参数信息
 *         in: formData #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *       - name: role_code
 *         description: 角色权限码
 *         type: string
 *         in: formData
 *         required: false
 *       - name: menu_ids
 *         description: 角色权限菜单
 *         type: string
 *         in: formData
 *         required: false
 *     responses: #返回结果
 *       200:
 *         description: 成功
 */
// 修改角色
router.put(
  "/role-update",
  roleMiddleware("role_update"),
  async (ctx: Koa.Context) => {
    const roleInfo = ctx.request.body as Role;

    await validatorData(roleInfo, Role);

    if (!roleInfo.role_id) {
      ctx.body = response(500, null, `角色ID不能为空！`);
      return;
    }

    const roleDetail = await roleFindOneByRoleId(roleInfo.role_id);

    if (!roleDetail) {
      ctx.body = response(500, null, `不存在该角色！`);
      return;
    }
    await roleUpdate(roleDetail);

    ctx.body = response(200, null, "操作成功！");
  },
);

/**
 * @swagger
 * /role-delete: #接口访问路径
 *   get: #请求方式
 *     summary: 删除角色  #接口信息
 *     description: 删除角色 #接口描述
 *     tags: [角色] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: ids #参数名字
 *         description: 角色id，逗号隔开，支持批量删除 #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 *       401:
 *         description: 未登录
 */
// 删除角色
router.delete(
  "/role-delete",
  roleMiddleware("role_delete"),
  async (ctx: Koa.Context) => {
    const ids = ctx.request.query.ids as string;
    if (!ids) {
      ctx.body = response(500, null, `角色ID不能为空！`);
      return;
    }

    await roleDelByRoleIds(ids.split(","));

    ctx.body = response(200, null, "操作成功！");
  },
);

/**
 * @swagger
 * /role-detail: #接口访问路径
 *   get: #请求方式
 *     summary: 角色详情  #接口信息
 *     description: 获取角色详情 #接口描述
 *     tags: [角色] #接口分组
 *     produces:
 *       - application/json
 *     parameters: #传递参数
 *       - name: id #参数名字
 *         description: 角色id #参数信息
 *         in: query #参数是存放到请求体中
 *         required: true #是否是必传参数
 *         type: string #参数类型
 *     responses: #返回结果
 *       200:
 *         description: 成功
 *         schema:
 *                  type: object
 *                  properties:
 *                    role_id:
 *                      type: integer
 *                      description: 角色id
 *                    role_name:
 *                      type: string
 *                      description: 角色名称
 *                    role_code:
 *                      type: string
 *                      description: 角色码
 *                    menu_ids:
 *                      type: string
 *                      description: 菜单ids
 *                    create_time:
 *                      type: string
 *                      description: 创建时间
 *       401:
 *         description: 未登录
 */
// 查询角色
router.get(
  "/role-detail",
  roleMiddleware("role_detail"),
  async (ctx: Koa.Context) => {
    const id = ctx.request.query.id as string;
    if (!id) {
      ctx.body = response(500, null, `角色ID不能为空！`);
      return;
    }

    ctx.body = response(200, await roleFindOneByRoleId(id), "操作成功！");
  },
);

// 查询角色

export default router;
