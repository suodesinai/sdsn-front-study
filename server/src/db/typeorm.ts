import { DataSource } from "typeorm";
console.log(__dirname + "/../entity/*.ts");

export const dataSource = new DataSource({
  type: "mysql",
  host: "localhost",
  port: 3306,
  username: "root",
  password: "root",
  database: "sdsn_koa",
  entities: [__dirname + "/../entity/*.{js,ts}"],
  logging: true, // 开启调试模式
});

dataSource
  .initialize()
  .then(async () => {
    console.log("Connection initialized with database...");
  })
  .catch((error) => console.log(error));
