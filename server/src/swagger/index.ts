import Koa from "koa";
const Router = require("koa-router");

const router = new Router();
const swaggerJSDoc = require("swagger-jsdoc");
const path = require("path");
const swaggerDefinition = {
  info: {
    title: "koa学习接口文档",
    version: "1.0.0",
    description: "API",
  },
  host: "localhost:3000",
  basePath: "/", // Base path (optional)
  securityDefinitions: {
    bearerAuth: {
      type: "apiKey",
      name: "token",
      scheme: "bearer",
      in: "header",
    },
  },
  security: [{ bearerAuth: [] }],
};
const options = {
  swaggerDefinition,
  apis: [path.join(__dirname, "../router/*")], //配置路由router文件的位置
};
const swaggerSpec = swaggerJSDoc(options);
// 通过路由获取生成的注解文件
router.get("/swagger.json", async function (ctx: Koa.Context) {
  ctx.set("Content-Type", "application/json");
  ctx.body = swaggerSpec;
});
module.exports = router;
