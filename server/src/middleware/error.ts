import Koa from "koa";
export default async function errorMiddleware(ctx: Koa.Context, next: Koa.Next) {
  try {
    await next()
  } catch (error:any) {
    ctx.status = 500
    ctx.body = {
        code:500,
        data:null,
        msg:error?error.message : "系统异常，请联系管理员！"
    }
  }
}