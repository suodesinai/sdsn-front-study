import Koa from "koa";
import log4js from "log4js";
export default async function useMiddleware(ctx: Koa.Context, next: Koa.Next) {
  log4js.configure({
    appenders: {
      // 配置一个 default 对象, type代表输出文件类型 filename代表输出文件名
      default: { type: "file", filename: "log/default.log" },
      // 配置了一个 cheese 对象 type代表输出文件类型 filename代表输出文件名
      cheese: { type: "file", filename: "log/cheese.log" },
    },
    categories: {
      // 默认 default 分类, 触发 debug 级别日志时会使用 default 对象处理
      default: { appenders: ["default"], level: "debug" },
      // 自定义一个 cheese 分类, 触发 error 级别日志时会使用 cheese 和 debug 对象处理
      cheese: { appenders: ["default", "cheese"], level: "error" },
    },
  });
  // 获取默认日志对象
  //   const logger = log4js.getLogger();
  //   logger.debug("只会使用 categories 对象中的 default 对象的配置处理该日志");

  // 获取自定义cheese对象日志
  const loggerCheese = log4js.getLogger("api");
  loggerCheese.debug(ctx.URL);

  await next();
}
