import http from "http";
import Koa from "koa";
import { initRouter } from "./router";
import { Ws } from "./servers/im/im";

const app = new Koa();
initRouter(app);
app.on("error", (err: any) => {
  console.error("server error", err);
});

// app.listen(3000, () => {
//   console.log("访问地址：http://localhost:3000");
//   console.log("swagger地址：http://localhost:3000/swagger");
// });

const server = http.createServer(app.callback());

new Ws(server);
