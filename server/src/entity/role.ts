import { IsNotEmpty } from "class-validator";
import moment from "moment";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  PrimaryColumn,
  OneToOne,
  JoinColumn,
} from "typeorm";

@Entity()
export class Role {
  @PrimaryGeneratedColumn({ name: "role_id" })
  role_id!: number;

  @Column({ name: "role_name" })
  @IsNotEmpty({ message: "角色名称不能为空！" })
  role_name!: string;

  @Column()
  role_code!: string;

  @Column()
  menu_ids!: string;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => value,
    },
  })
  create_time?: string;
}

@Entity()
export class RoleUser {
  @PrimaryGeneratedColumn()
  role_id!: number;

  @PrimaryColumn()
  user_id!: number;

  @OneToOne(() => Role)
  @JoinColumn({ name: "user_id" })
  role!: Role;
}
