import { IsNotEmpty } from "class-validator";
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Org {
  @PrimaryGeneratedColumn({ name: "org_id" })
  org_id?: number;

  @Column({ name: "org_name" })
  @IsNotEmpty({ message: "组织名称不能为空！" })
  org_name?: string;

  @Column()
  parent_id?: number;

  @Column()
  org_code?: string;
}
