import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
  OneToOne,
} from "typeorm";
import { IsNotEmpty } from "class-validator";
import moment from "moment";
import { User } from "./user";

@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  task_id!: number;

  @Column({ comment: "任务名称", name: "task_name" })
  @IsNotEmpty({ message: "任务名称不能为空！" })
  task_name!: string;

  @Column({ comment: "任务分类", name: "task_category" })
  @IsNotEmpty({ message: "所属分类不能为空！" })
  task_category!: number;

  @Column({ comment: "任务状态", name: "task_status" })
  task_status!: string;

  @Column({ comment: "任务类型", name: "task_type" })
  task_type!: string;

  @Column({ comment: "创建者", name: "create_user" })
  create_user!: number;

  @Column({ comment: "任务内容", name: "task_content" })
  task_content!: string;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => moment(value || new Date()).format("YYYY-MM-DD HH:mm:ss"),
    },
  })
  create_time?: string;

  @OneToMany(() => TaskUser, (taskUser) => taskUser.task)
  taskUsers?: TaskUser[];

  @OneToOne(() => User)
  @JoinColumn({ name: "create_user" })
  create_user_info?: User;
}

// 任务参与者
@Entity({ name: "task_user" })
export class TaskUser {
  @PrimaryGeneratedColumn({ name: "task_user_id" })
  task_user_id?: number;

  @Column({ name: "task_id" })
  task_id?: number;

  @Column({ name: "user_id" })
  user_id?: number;

  @ManyToOne(() => Task, (task) => task.taskUsers)
  @JoinColumn({ name: "task_id" })
  task?: Task[];
}

// 任务备注
@Entity({ name: "task_remark" })
export class TaskRemark {
  @PrimaryGeneratedColumn({ name: "remark_id" })
  remark_id?: number;

  @Column({ name: "remark_content" })
  @IsNotEmpty({ message: "备注不能为空！" })
  remark_content?: string;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => moment(value || new Date()).format("YYYY-MM-DD HH:mm:ss"),
    },
  })
  create_time?: string;

  @Column({ name: "create_user" })
  create_user?: number;

  @Column({ name: "task_id" })
  @IsNotEmpty({ message: "任务ID不能为空！" })
  task_id?: number;

  @OneToOne(() => User)
  @JoinColumn({ name: "create_user" })
  create_user_info?: User;
}
