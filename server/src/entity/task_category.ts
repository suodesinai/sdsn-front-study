import { IsNotEmpty } from "class-validator";
import moment from "moment";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  JoinColumn,
  JoinTable,
} from "typeorm";

@Entity({ name: "task_category" })
export class TaskCategory {
  @PrimaryGeneratedColumn({ name: "category_id" })
  category_id?: number;

  @Column({ name: "category_name" })
  @IsNotEmpty({ message: "分类名称不能为空！" })
  category_name?: string;

  @Column({ name: "category_describe" })
  category_describe?: string;

  @Column({ name: "create_user" })
  create_user?: number;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => {
        return moment(value).format("YYYY-MM-DD HH:mm:ss");
      },
    },
    name: "create_time",
  })
  create_time?: string;

  @OneToMany(
    () => TaskCategoryPerm,
    (taskCategoryPerm) => taskCategoryPerm.taskCategory,
    {
      cascade: ["remove"],
    },
  )
  taskCategoryPerms?: TaskCategoryPerm[];
}

@Entity({ name: "task_category_perm" })
export class TaskCategoryPerm {
  @PrimaryGeneratedColumn({ name: "task_category_perm_id" })
  task_category_perm_id?: number;

  @Column({ name: "category_id" })
  category_id?: number;

  @Column({ name: "permission_id" })
  permission_id?: number;

  @Column({ name: "permission_type" })
  permission_type?: string;

  @ManyToOne(
    () => TaskCategory,
    (taskCategory) => taskCategory.taskCategoryPerms,
  )
  @JoinColumn({ name: "category_id" })
  taskCategory?: TaskCategory;
}
