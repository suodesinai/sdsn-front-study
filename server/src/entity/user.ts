import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  PrimaryColumn,
} from "typeorm";
import { Role } from "./role";
import { IsEmpty, IsNotEmpty, IsString } from "class-validator";
import moment from "moment";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  user_id!: number;

  @Column({ comment: "账号", name: "user_name" })
  @IsNotEmpty({ message: "账号不能为空！" })
  user_name!: string;

  @Column({ comment: "名称" })
  @IsNotEmpty({ message: "名称不能为空！" })
  user_nick!: string;

  @Column({ select: false })
  password?: string;

  @Column()
  status?: string;

  @Column({ nullable: false })
  phone?: string;

  @Column()
  email?: string;

  @Column()
  sex?: string;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => moment(value || new Date()).format("YYYY-MM-DD HH:mm:ss"),
    },
  })
  create_time?: string;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => {
        if (value) {
          return moment(value).format("YYYY-MM-DD HH:mm:ss");
        }
      },
    },
    name: "login_time",
  })
  login_time?: string;

  @Column()
  remark?: string;
}

@Entity()
export class UserRole {
  @PrimaryGeneratedColumn()
  user_id!: number;

  @PrimaryColumn()
  role_id!: number;

  @OneToOne(() => Role)
  @JoinColumn({ name: "role_id" })
  role?: Role;
}

export type UserContainer = User & {
  roles?: string;
};
