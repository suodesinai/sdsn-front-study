import { IsNotEmpty } from "class-validator";
import moment from "moment";
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToOne,
  JoinColumn,
  OneToMany,
  ManyToOne,
} from "typeorm";

@Entity()
export class ArticleCategory {
  @PrimaryGeneratedColumn({ name: "category_id" })
  category_id?: number;

  @Column({ name: "category_name" })
  @IsNotEmpty({ message: "分类名称不能为空！" })
  category_name?: string;

  @Column({ name: "create_user" })
  @IsNotEmpty({ message: "创建者ID不能为空！" })
  create_user?: number;

  @Column({ name: "parent_id" })
  parent_id?: number;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => {
        return moment(value).format("YYYY-MM-DD HH:mm:ss");
      },
    },
    name: "create_time",
  })
  create_time?: number;

  @OneToMany(
    () => ArticleCategoryRole,
    (articleCategoryRole) => articleCategoryRole.articleCategory,
  )
  articleCategoryRoles?: ArticleCategoryRole[];
}

@Entity({ name: "articlecategory_role" })
export class ArticleCategoryRole {
  @PrimaryGeneratedColumn({ name: "category_id" })
  category_id?: number;

  @Column({ name: "role_id" })
  role_id?: string;

  @ManyToOne(
    () => ArticleCategory,
    (articleCategory) => articleCategory.articleCategoryRoles,
  )
  @JoinColumn({ name: "category_id" })
  articleCategory?: ArticleCategory[];
}

@Entity({ name: "articlecategory_role" })
export class ArticleRoleCategory {
  @PrimaryGeneratedColumn({ name: "role_id" })
  role_id?: number;

  @Column({ name: "category_id" })
  category_id?: string;

  @OneToOne(() => ArticleCategory)
  @JoinColumn({ name: "category" })
  category?: ArticleCategory;
}

@Entity({ name: "articlecategory_role" })
export class ArticleRoleCategoryModel {
  @PrimaryGeneratedColumn({ name: "role_id" })
  role_id?: number;

  @Column({ name: "category_id" })
  category_id?: string;
}
