import { IsNotEmpty } from "class-validator";
import moment from "moment";
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

// 文章表
@Entity()
export class Article {
  @PrimaryGeneratedColumn({ name: "article_id" })
  article_id?: number;

  @Column({ name: "title" })
  @IsNotEmpty({ message: "标题不能为空！" })
  title?: string;

  @Column()
  @IsNotEmpty({ message: "内容不能为空！" })
  content?: string;

  @Column()
  article_thumb?: string;

  @Column()
  @IsNotEmpty({ message: "所属分类不能为空！" })
  category_id?: string;

  @Column()
  create_user?: string;

  @Column()
  reads?: number;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => moment(value || new Date()).format("YYYY-MM-DD HH:mm:ss"),
    },
  })
  create_time?: string;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => {
        if (value) {
          return moment(value).format("YYYY-MM-DD HH:mm:ss");
        }
      },
    },
    name: "update_time",
  })
  update_time?: string;
}

// 文章与文件中间表
@Entity()
export class ArticleFile {
  @PrimaryGeneratedColumn({ name: "article_id" })
  article_id?: number;

  @Column()
  file_id?: string;
}
