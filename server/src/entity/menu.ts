import { IsNotEmpty } from "class-validator";
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Menu {
  @PrimaryGeneratedColumn({ name: "menu_id" })
  menu_id?: number;

  @Column({ name: "menu_name" })
  @IsNotEmpty({ message: "菜单名称不能为空！" })
  menu_name?: string;

  @Column()
  parent_id?: number;

  @Column()
  @IsNotEmpty({ message: "菜单编码不能为空！" })
  menu_code?: string;

  @Column()
  @IsNotEmpty({ message: "菜单地址不能为空！" })
  menu_path?: string;
}
