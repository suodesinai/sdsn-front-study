import { IsNotEmpty } from "class-validator";
import moment from "moment";
import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

// 网盘表
@Entity()
export class Netdisk {
  @PrimaryGeneratedColumn({ name: "netdisk_id" })
  netdisk_id?: number;

  @Column({ name: "file_name" })
  @IsNotEmpty({ message: "名称不能为空！" })
  file_name?: string;

  @Column()
  file_type?: string;

  @Column()
  create_user?: string;

  @Column()
  file_status?: string;

  @Column()
  file_path?: string;

  @Column()
  parent_id?: string;

  @Column("datetime", {
    transformer: {
      from: (value) => moment(value).format("YYYY-MM-DD HH:mm:ss"),
      to: (value) => moment(value || new Date()).format("YYYY-MM-DD HH:mm:ss"),
    },
  })
  create_time?: string;
}
