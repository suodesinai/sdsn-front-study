export interface CreateCategory {
  category_id?: number;
  category_name: string;
  parent_id: number;
  role_id: string;
  role_name: string;
  create_user: number;
}
