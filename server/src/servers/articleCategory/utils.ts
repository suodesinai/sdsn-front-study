import { ArticleCategoryRole } from "../../entity/article_category";

// 检查分类权限和用户权限是否有重合
export function checkIsRepeat(
  data?: ArticleCategoryRole[],
  roles?: number[],
): boolean {
  if (!data || !roles || data.length === 0 || roles.length === 0) {
    return false;
  } else {
    let setArr = new Set(
      data.map((item) => Number(item.role_id)).concat(roles),
    );
    if (Array.from(setArr).length < data.length + roles.length) {
      return true;
    } else {
      return false;
    }
  }
}

// 扁平数据转树型结构
export function listToTree(data: any, parentId = 0) {
  const temp: any = [];

  data.forEach((item: any) => {
    if (item.parent_id === parentId) {
      const children = listToTree(data, item.category_id);
      if (children.length) {
        item.children = children;
      }
      delete item.articleCategoryRoles;
      temp.push(item);
    }
  });

  return temp;
}
