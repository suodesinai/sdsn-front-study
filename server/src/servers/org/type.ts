export interface OrgTree {
  org_id: number;
  org_name: string;
  parent_id: number;
  org_code: string;
  children: OrgTree[];
}
