import { OrgTree } from "./type";

/**
 * 组织扁平化转树
 * @param orgs
 */
export function orgArrayToTree(orgs: OrgTree[]) {
  let orgsMap: { [key: string]: any } = {};

  for (let i = 0; i < orgs.length; i++) {
    if (orgsMap[orgs[i].parent_id]) {
      orgsMap[orgs[i].parent_id].push(orgs[i]);
    } else {
      orgsMap[orgs[i].parent_id] = [orgs[i]];
    }
  }

  orgs.forEach((item) => {
    if (orgsMap[item.parent_id]) {
      item.children = orgsMap[item.org_id];
    }
  });

  return orgsMap[0] || [];
}
