import { getJwtInfo } from "../../middleware/jwt";

const WebSocket = require("ws");

// 创建连接
export class Ws {
  static online = 0; // 在线人数
  constructor(server: any) {
    // 创建 WebSocket 服务器，使用相同的 HTTP 服务器实例
    const wss = new WebSocket.Server({ server, path: "/im" });

    // 监听 WebSocket 连接事件
    wss.on("connection", async (ws: any, request: any) => {
      const headerParams = request.headers["token"];
      if (headerParams && !(await getJwtInfo(headerParams, "suodesinai")).err) {
        // 监听来自客户端的消息
        ws.on("message", (message: any) => {
          console.log("Received message: %s", message);
          // 向客户端发送消息
          ws.send(`Echo: 我可以提前下班啦`);
        });
      } else {
        console.log("非法用户！");

        ws.close(500, "未知身份！");
      }

      // 监听连接关闭事件
      ws.on("close", () => {
        console.log("WebSocket connection closed");
      });
    });

    // 启动
    server.listen(3000, () => {
      console.log("访问地址：http://localhost:3000");
      console.log("swagger地址：http://localhost:3000/swagger");
    });
  }
}
