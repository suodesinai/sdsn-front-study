import { MenuTree } from "./type";

/**
 * 菜单扁平化转树
 * @param menus
 */
export function menuArrayToTree(menus: MenuTree[]) {
  let menusMap: { [key: string]: any } = {};

  for (let i = 0; i < menus.length; i++) {
    if (menusMap[menus[i].parent_id]) {
      menusMap[menus[i].parent_id].push(menus[i]);
    } else {
      menusMap[menus[i].parent_id] = [menus[i]];
    }
  }

  menus.forEach((item) => {
    if (menusMap[item.parent_id]) {
      item.children = menusMap[item.menu_id];
    }
  });

  return menusMap[0] || [];
}
