export interface MenuTree {
  menu_id: number;
  menu_name: string;
  parent_id: number;
  menu_code: string;
  menu_path?: string;
  children: MenuTree[];
}
