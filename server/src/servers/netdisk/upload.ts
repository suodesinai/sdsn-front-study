import Koa from "koa";
const multer = require("@koa/multer");

const storage = multer.diskStorage({
  destination: function (
    req: Koa.Request,
    file: File,
    cb: (arg0: null, arg1: string) => void,
  ) {
    cb(null, "netdisk/files/"); // 确保这个文件夹已经存在
  },
  filename: function (
    req: Koa.Request,
    file: {
      [x: string]: string;
      fieldname: string;
    },
    cb: (arg0: null, arg1: string) => void,
  ) {
    // 这里文件名有问题，后期更换成类似雪花算法id
    cb(null, new Date().getTime() + "_" + file.originalname);
  },
});
export const upload = multer({ storage: storage });
