interface ResponseBody {
  code?: number;
  data?: unknown;
  msg?: string;
}

export function response(
  code: number = 200,
  data: any = null,
  msg: string = "ok！",
): ResponseBody {
  return {
    code,
    data,
    msg,
  };
}
