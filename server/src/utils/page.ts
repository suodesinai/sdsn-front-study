
const mysql = require("mysql2");
const db = require("../db/mysql");
export class SqlUtil {
  constructor() {}

  table = "";
  order = "";
  feild = "*";
  wheres: string[] = [];
  lefts: string[] = [];
  rights: string[] = [];

  Table(name: string) {
    this.table = name;
    return this;
  }

  Feild(str: string) {
    this.feild = str;
    return this
  }

  Order(str: string) {
    this.order = str;
    return this;
  }

  Left(str: string) {
    this.lefts.push(str);
    return this
  }

  Right(str: string) {
    this.rights.push(str);
    return this
  }

  Where(strs:string,...arg: unknown[]) {
    
    if (arg.length === 1) {
      strs = strs.replace("?",mysql.escape(arg[0] as unknown as any))
    }

    this.wheres.push(strs);
    return this
  }

  Update<T>(data:any){}

  async Find() {
    let sql = `select ${this.feild} from ${this.table}`;

    if (this.lefts.length > 0) {
      sql = sql + `${this.lefts.join(" ")}`;
    }

    if (this.wheres && this.wheres.length > 0) {
      sql = sql + ` where ${this.wheres.join(" and ")}`;
    }

    return await db(sql);
  }

  async FindOne() {
    let sql = `select ${this.feild} from ${this.table}`;

    if (this.lefts.length > 0) {
      sql = sql + ` ${this.lefts.join(" ")}`;
    }

    if (this.rights.length > 0) {
      sql = sql + ` ${this.rights.join(" ")}`;
    }

    if (this.wheres && this.wheres.length > 0) {
      sql = sql + ` where ${this.wheres.join(" and ")}`;
    }

    let result = await db(sql)

    return result && result.length > 0?result[0]:null;
  }

  async Insert<T>(data:T){
    let sql:string = "";

    if (!data || Object.keys(data).length === 0) {
      console.error("数据插入不能为空！");
      return false
    }

    let insertKeys:string[]= [];
    let insertValues:unknown[]= [];

    for (const key in data) {
      if (Object.prototype.hasOwnProperty.call(data, key)) {
        insertKeys.push(key)        
        insertValues.push(`"${data[key]}"`)     
      }
    }

    if (insertKeys.length > 0) {
      sql = `insert into ${this.table} (${insertKeys.join(",")}) values (${insertValues.join(",")})`
    }

    if (!sql) {
      console.error("sql不能为空！");
      return false
    }

    return await db(sql)
    
  }
}



export class PageUtil extends SqlUtil{

  current = 1;
  pageSize = 10;

  FilterPage(query: { [key: string]: any } = {}) {
    this.current = query.pageIndex || 1;
    this.pageSize = query.pageSize || 10;
    return this;
  }

  Page(index: number, size: number) {
    this.current = index;
    this.pageSize = size;
  }

  async Find() {
    let sql = `select ${this.feild} from ${this.table}`;

    if (this.lefts.length > 0) {
      sql = sql + `${this.lefts.join(" ")}`;
    }

    if (this.wheres && this.wheres.length > 0) {
      sql = sql + ` where ${this.wheres.join(" and ")}`;
    }

    let endIndex = 10;
    let startIndex = 0;

    startIndex = (this.current - 1) * this.pageSize;
    endIndex = this.current * this.pageSize;

    sql = sql + ` limit ${startIndex},${endIndex}`;

    const records = await db(sql)

    const total = await db(`select count(*) from ${this.table}` + (this.wheres.length > 0?` where ${this.wheres.join(" and ")}`:``))
    
    return {records,total:total[0]["count(*)"]};
  }

  async FindOne() {
    let sql = `select ${this.feild} from ${this.table}`;

    if (this.lefts.length > 0) {
      sql = sql + `${this.lefts.join(" ")}`;
    }

    if (this.wheres && this.wheres.length > 0) {
      sql = sql + ` where ${this.wheres.join(" and ")}`;
    }

    let endIndex = 10;
    let startIndex = 0;

    startIndex = (this.current - 1) * this.pageSize;
    endIndex = this.current * this.pageSize;

    sql = sql + ` limit ${startIndex},${endIndex}`;

    let result = await db(sql)

    return result && result.length>0?result[0]:null;
  }
}