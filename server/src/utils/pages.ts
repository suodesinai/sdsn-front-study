export function formatPage(query: any) {
  let pageIndex, pageSize;
  if (query) {
    pageIndex = query.pageIndex || 1;
    pageSize = query.pageSize || 10;
  } else {
    pageIndex = 1;
    pageSize = 10;
  }

  return { pageIndex, pageSize };
}

export function formatPageFileterData(query: any, fields?: string[]) {
  let data: { [key: string]: unknown } = {};
  if (query) {
    for (const key in query) {
      if (Object.prototype.hasOwnProperty.call(query, key)) {
        if (
          !["pageSize", "pageIndex"].includes(key) &&
          (!fields || fields.length === 0 || fields.includes(key))
        ) {
          data[key] = JSON.parse(JSON.stringify(query[key]));
        }
      }
    }
  }
  return data;
}
