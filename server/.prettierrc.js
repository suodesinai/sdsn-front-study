module.exports = {
  tabwidth: 8,
  singleQuote: false, // 使用单引号
  htmlWhitespaceSensitivity: "ignore",
  semi: true, // 结尾不用分号
  trailingComma: "all", // 函数最后不需要逗号
};
