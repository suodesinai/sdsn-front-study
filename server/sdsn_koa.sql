-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.22-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  11.2.0.6293
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- 导出 sdsn_koa 的数据库结构
DROP DATABASE IF EXISTS `sdsn_koa`;
CREATE DATABASE IF NOT EXISTS `sdsn_koa` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `sdsn_koa`;

-- 导出  表 sdsn_koa.org 结构
DROP TABLE IF EXISTS `org`;
CREATE TABLE IF NOT EXISTS `org` (
  `org_id` int(11) NOT NULL AUTO_INCREMENT,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `org_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- 正在导出表  sdsn_koa.org 的数据：~1 rows (大约)
DELETE FROM `org`;
/*!40000 ALTER TABLE `org` DISABLE KEYS */;
INSERT INTO `org` (`org_id`, `create_time`, `org_name`) VALUES
	(1, '2024-01-21 00:43:35', '开发部');
/*!40000 ALTER TABLE `org` ENABLE KEYS */;

-- 导出  表 sdsn_koa.role 结构
DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(200) NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `role_code` text,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- 正在导出表  sdsn_koa.role 的数据：~2 rows (大约)
DELETE FROM `role`;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`role_id`, `role_name`, `create_time`, `role_code`) VALUES
	(1, '超管', '2024-01-21 00:39:23', 'user_edit,user_add'),
	(2, '管理员', '2024-01-19 17:07:24', 'user_edit');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- 导出  表 sdsn_koa.role_code 结构
DROP TABLE IF EXISTS `role_code`;
CREATE TABLE IF NOT EXISTS `role_code` (
  `code` varchar(20) NOT NULL,
  `message` varchar(100) NOT NULL,
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  sdsn_koa.role_code 的数据：~2 rows (大约)
DELETE FROM `role_code`;
/*!40000 ALTER TABLE `role_code` DISABLE KEYS */;
INSERT INTO `role_code` (`code`, `message`) VALUES
	('user_edit', '用户编辑'),
	('user_add', '新增用户');
/*!40000 ALTER TABLE `role_code` ENABLE KEYS */;

-- 导出  表 sdsn_koa.user 结构
DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(200) NOT NULL,
  `password` varchar(37) NOT NULL,
  `create_time` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- 正在导出表  sdsn_koa.user 的数据：~1 rows (大约)
DELETE FROM `user`;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`user_id`, `user_name`, `password`, `create_time`) VALUES
	(1, 'admin', '123456', '2024-01-19 16:36:36.000000');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- 导出  表 sdsn_koa.user_org 结构
DROP TABLE IF EXISTS `user_org`;
CREATE TABLE IF NOT EXISTS `user_org` (
  `user_id` int(11) NOT NULL,
  `org_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `org_id` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  sdsn_koa.user_org 的数据：~1 rows (大约)
DELETE FROM `user_org`;
/*!40000 ALTER TABLE `user_org` DISABLE KEYS */;
INSERT INTO `user_org` (`user_id`, `org_id`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `user_org` ENABLE KEYS */;

-- 导出  表 sdsn_koa.user_role 结构
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE IF NOT EXISTS `user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  KEY `user_id` (`user_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 正在导出表  sdsn_koa.user_role 的数据：~2 rows (大约)
DELETE FROM `user_role`;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
INSERT INTO `user_role` (`user_id`, `role_id`) VALUES
	(1, 2),
	(1, 1);
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
