
import { createRouter, createWebHistory } from "vue-router"

const routes = [
    { 
        path: '/', 
        component: ()=>import("@/views/index.vue"),
        children:[
            { path: '/about', component: ()=>import("@/views/about.vue") },
            { path: '/contact', component: ()=>import("@/views/contact.vue") },
        ]
    },
    {
        path:"/login",
        component: ()=>import("@/views/login.vue"),
    }
]
  
export const router = createRouter({
    history: createWebHistory(),
    routes,
})