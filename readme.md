## 项目介绍

- front 前端vue项目
- server 后端koa服务



# 技术栈：Koa+Mysql+Vue3+TypeScript+Swagger

# 命令

### 后端/server
```
npm run dev  // 启动

npm run build // 打包
```

# swagger文档

```
http://localhost:3000/swagger
```

# 项目介绍

- src/router 路由
- src/middleware 中间件（登录拦截/log日志）
- src/types typescript类型
- src/utils 工具类（函数方法）
- src/index 入口文件
